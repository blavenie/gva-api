import {DBTx} from "duniter/app/lib/db/DBTx";
import {Server} from "duniter/server";
import {uniqueWithExclusion} from "./utils";

export declare interface Movement {
  type: 'T' | 'D';
  version: number;
  currency: string;
  comment: string;
  pubkeys: string[];
  amount: number;
  hash: string;

  blockstamp: string;
  time?: number | null;

  block: string;
  // Null if the document is not written in blockchain
  //written_on?: BlockRef | null;
}

export function txInputOrOutputAmount(output: string) {
  const parts = output.split(':');
  return +parts[0] * Math.pow(10, +parts[1]);
}

export function txAmountForPubkey(tx: DBTx, pubkey:string){
  const issuerIndex = tx.issuers.findIndex(i => i === pubkey);
  const inputAmount = (issuerIndex === -1) ? 0 : tx.unlocks.reduce((sum, unlock, i) => {
    return sum + (unlock.endsWith(`:SIG(${issuerIndex})`) && txInputOrOutputAmount(tx.inputs[i]) || 0);
  }, 0);
  const outputAmount = tx.outputs.reduce((sum, output) => {
    return sum + (output.endsWith(`:SIG(${pubkey})`) && txInputOrOutputAmount(output) || 0);
  }, 0);
  return outputAmount - inputAmount;
}
export function txAmount(tx: DBTx){
  return tx.issuers.reduce((sum, issuer) => {
    return sum + txAmountForPubkey(tx, issuer);
  }, 0);
}

/**
 * Return pubkeys (issuers or recipients)
 * @param server
 * @param tx
 * @param pubkey
 */

export function txToMovement(server: Server, tx: DBTx, pubkey: string): Movement {
  const amount = txAmountForPubkey(tx, pubkey);
  return {
    version: tx.version,
    currency: tx.currency,
    type: 'T',
    comment: tx.comment,
    amount: amount,
    pubkeys: amount > 0 ? uniqueWithExclusion(tx.issuers, pubkey) : uniqueWithExclusion(tx.recipients, pubkey),
    blockstamp: tx.blockstamp,
    time: tx.blockstampTime && (tx.blockstampTime /*medianTime*/ + server.conf.avgGenTime * server.conf.medianTimeBlocks) || null,
    hash: tx.hash,
    block: tx.blockstamp
    //written_on: tx.written_on
  }
}
