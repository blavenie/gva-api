import {plugModule} from "./gva";
import * as express from "express";
import graphqlHTTP = require("express-graphql");
import {Server} from "duniter/server";
import * as http from "http";
const logger_1 = require("duniter/app/lib/logger");
const cors = require('cors');

export function gvaHttpListen(server: Server, port: number, host = 'localhost'): http.Server {
  const logger = logger_1.NewLogger('gva');
  const app = express();

  // Enable `cors` to set HTTP response header: Access-Control-Allow-Origin: *
  app.use(cors());

  app.use('/graphql', graphqlHTTP({
    schema: plugModule(server, logger),
    graphiql: true,
  }));

  return app.listen(port, host,
      () => {
        logger && logger.info(`GVA server listening on http${port==443 ? 's' : ''}://${host}:${port}/graphql`);
      });
}
