export declare class Block {
  number: number;
  // Block hash
  hash: string;

  // Block time
  time: number;

  /**
   * Block median time
   * @Deprecated use commonTime
   */
  medianTime: number;

  /**
   * Blockchain (average) time
   */
  commonTime: number;

  // humanTime = medianTime + (avgGenTime * medianTimeBlocks / 2)
  // TODO check the RFC final name
  humanTime: number;

  // TODO: check if define in the RFC
  issuerName: string;
}
