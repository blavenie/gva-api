/**
 * Usage example:
 * <code>
 *     var a = ['a', 1, 'a', 2, '1'];
 *     var unique = a.filter( onlyUnique ); // returns ['a', 1, 2, '1']
 * </code>
 * @param value
 * @param index
 * @param self
 */
export function onlyUnique<T>(value: T, index: number, self: T[]) {
    return self.indexOf(value) === index;
}

/**
 * Usage example:
 * <code>
 *     var a = ['a', 1, 'a', 2, '1'];
 *     var unique = uniqueWithExclusion(a, 1); // returns ['a', 2, '1']
 * </code>
 * @param values
 * @param valueToExclude
 */
export function uniqueWithExclusion<T>(values: T[], valueToExclude: T): T[]{
    return (values||[])
        .filter((value, index, self) => {
            // Exclude given value
            return value !== valueToExclude
                // Avoid duplicated issuer
                && self.indexOf(value) === index;
        });
}
